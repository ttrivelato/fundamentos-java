package br.com.igordev.teste;

import br.com.igordev.dominio.Cliente;
import br.com.igordev.dominio.Produto;
import br.com.igordev.dominio.Venda;
import java.util.Date;
import java.util.Scanner;

public class TesteVenda {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("[ SISTEMA DE E-COMMERCE KA ]\n\n");
        Cliente cliente = new Cliente();
        System.out.print("Cod.Cliente: ");
        cliente.codigo = sc.nextInt();
        System.out.print("Nome: ");
        cliente.nome = sc.next();
        System.out.print("Rua: ");
        cliente.enderecoEntrega = sc.next();
        System.out.print("Num. rua: ");
        cliente.enderecoEntrega += ", " + sc.nextInt();
        System.out.print("Quantos produtos: ");
        int qtdProdutos = sc.nextInt();
        //informações da venda
        Venda venda = new Venda();
        System.out.print("Cod.Venda: ");
        venda.codigo = sc.nextInt();
        venda.data = new Date(); //pega data automático
        venda.cliente = cliente;
        venda.produtos = new Produto[qtdProdutos];
        for (int i = 0;i < qtdProdutos; i++) {
            venda.produtos[i] = new Produto();
            System.out.print("Cod.Produto: ");
            venda.produtos[i].codigo = sc.nextInt();
            System.out.print("Descrição: ");
            venda.produtos[i].nome = sc.next();
            System.out.print("Valor: ");
            venda.produtos[i].valor = sc.nextDouble();
        } //fim for
        venda.calculaTotal(); //calcular total da venda;
        venda.imprime();
    }

}
