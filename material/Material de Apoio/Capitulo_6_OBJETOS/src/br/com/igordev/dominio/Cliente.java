package br.com.igordev.dominio;

public class Cliente {
    //atributo
    public int codigo;
    public String nome;
    public String enderecoEntrega;

    //metodos
    public void imprime() {
        System.out.println("Cliente: #" + codigo);
        System.out.println("Nome: " + nome);
        System.out.println("End.Entrega: " + enderecoEntrega);
    }
}
