/*
Programador: Igor
Data: 25/03/2019
Primeiro programa do curso de Java
*/

//antes de package só podem existir comentários
package br.com.kasolution.teste;

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Olá mundo Java!");
    } //fim main
}//fim classe
