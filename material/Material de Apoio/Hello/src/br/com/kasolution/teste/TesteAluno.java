package br.com.kasolution.teste;

import br.com.kasolution.dominio.Aluno;

public class TesteAluno {

    public static void main(String[] args) {
        Aluno aluno1;
        aluno1 = new Aluno();
        aluno1.nome = "Felipe Ribeiro";
        aluno1.idade = 20;
        //escrevendo os dados
        System.out.println("Nome: " + aluno1.nome);
        System.out.println("Idade: " + aluno1.idade);
    }
}
