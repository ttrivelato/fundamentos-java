package br.com.kasolution.dominio;

public class FuncionarioAssalariado extends Funcionario {

    private double salarioSemanal;
    
    public FuncionarioAssalariado(double salarioSemanal,
            String nome, String sobrenome, String cpf) {
        super(nome, sobrenome, cpf);
        this.salarioSemanal = salarioSemanal;
    }

    public double getSalarioSemanal() {
        return salarioSemanal;
    }

    public void setSalarioSemanal(double salarioSemanal) {
        this.salarioSemanal = salarioSemanal;
    }

    @Override
    public void imprime() {
        super.imprime();
        System.out.println("Sal.Semanal: " + salarioSemanal);
    }

    @Override
    public double calculaValorPagamento() {
        double total = salarioSemanal; //não tem calculo
        return total;
    }
}







