package br.com.kasolution.classe.anonima;

import java.math.BigDecimal;

@FunctionalInterface public interface Operacao {
	public BigDecimal efetua(BigDecimal valorA, BigDecimal valorB);
	
	
}
