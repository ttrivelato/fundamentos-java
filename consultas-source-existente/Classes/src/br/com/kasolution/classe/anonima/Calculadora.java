package br.com.kasolution.classe.anonima;

import java.math.BigDecimal;

public class Calculadora {
	public BigDecimal calcula(BigDecimal valorA, BigDecimal valorB, Operacao operacao) {
		return operacao.efetua(valorA, valorB);
		
	}
	
}
