package br.com.kasolution.classe.anonima;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TesteCalculadora {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Calculadora c = new Calculadora();
		
		BigDecimal v1 = new BigDecimal(50);
		BigDecimal v2 = new BigDecimal(60);
		
		BigDecimal rAdicao = c.calcula(v1, v2, new Adicao());
		System.out.println("Resultado Adi��o: " + rAdicao);
		
		BigDecimal rSubtracao = c.calcula(v1, v2, new Operacao() {
			@Override
			public BigDecimal efetua(BigDecimal valorA, BigDecimal valorB) {
				// TODO Auto-generated method stub
				return valorA.subtract(valorB);
			}
		});
		
		System.out.println("Resultado subtra��o: " + rSubtracao);
		
		BigDecimal rMultiplicacao = c.calcula(v1, v2, (vA, vB) -> vA.multiply(vB));
		
		System.out.println("Resultado Multiplica��o: " + rMultiplicacao);
		
		BigDecimal rDivisao = c.calcula(v1, v2, (vA, vB) -> vA.divide(vB, 2, RoundingMode.HALF_UP));
		
		System.out.println("Resultado Divis�o: " + rDivisao);
	}

}
