package br.com.kasolution.classe.membro;

import java.time.LocalDate;

public class TesteFuncionario {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Funcionario f = new Funcionario("Guilherme", 2000.00);
		f.adicionaPeriodo("Banco de Dados", LocalDate.of(2017, 2, 1), LocalDate.of(2017,4,30));
		f.adicionaPeriodo("Rede", LocalDate.of(2017, 5, 1), LocalDate.of(2018, 12, 31));
		f.adicionaPeriodo("Desenvolvimento", LocalDate.of(2019, 1, 1), LocalDate.now());
		System.out.println(f);

	}

}
