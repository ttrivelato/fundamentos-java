package br.com.kasolution.classe.membro;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import br.com.kasolution.util.Formata;

public class Funcionario {
	private String nome;
	private double salario;
	private List<Periodo> historico = new ArrayList<>();
	
	public Funcionario(String nome, double salario) {
		super();
		this.nome = nome;
		this.salario = salario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	@Override
	public String toString() {
		String info = "Funcionario: " + nome;
		info += "\n Sal�rio: " + Formata.moeda(salario);
		for (Periodo p : historico) {
			info += "\n" + p + "\n";
		}
		return info;
	}
	
	public void adicionaPeriodo(String departamento, LocalDate dataInicio, LocalDate dataFim) {
		Periodo p = new Periodo(departamento, dataInicio, dataFim);
		historico.add(p);
	}
	
	private class Periodo {
		private String departamento;
		private LocalDate dataInicio;
		private LocalDate dataFim;
		
		public Periodo(String departamento, LocalDate dataInicio, LocalDate dataFim) {
			super();
			this.departamento = departamento;
			this.dataInicio = dataInicio;
			this.dataFim = dataFim;
		}

		@Override
		public String toString() {
			String info = "\tDepartamento: " + departamento;
			info+= "\t\nPeriodo: ";
			info += Formata.data(dataInicio) + " - " + Formata.data(dataFim);
			return info;
		}
		
		
		
		
	}

	
	
}
