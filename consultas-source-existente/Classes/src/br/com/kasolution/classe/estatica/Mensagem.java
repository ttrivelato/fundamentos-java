package br.com.kasolution.classe.estatica;

public class Mensagem {
	private static String texto;
	
	public static class Texto{
		public Texto (String texto) {
			Mensagem.texto = texto;
		}

		public void escreve() {
			System.out.println(Mensagem.texto);
	
	}

		
	}
}
