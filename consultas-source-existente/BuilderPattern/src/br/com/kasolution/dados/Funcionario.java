package br.com.kasolution.dados;

public class Funcionario {
	private Integer codigo;
	private String nome;
	
	public static class Builder {
		private Integer codigo = -1;
		private String nome = "--sem nome--";
		
		public Funcionario.Builder codigo(Integer codigo){
			this.codigo = codigo;
			return this;
		}
		
		public Funcionario.Builder nome(String nome){
			this.nome = nome;
			return this;
		}
		
		public Funcionario build() {
			return new Funcionario(this);
		}
	}
	
	private Funcionario (Funcionario.Builder builder) {
		this.codigo = builder.codigo;
		this.nome = builder.nome;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getNome() {
		return nome;
	}

	@Override
	public String toString() {
		return "Funcionario [codigo=" + codigo + ", nome=" + nome + "]";
	}
	
	
	
	
	
}
