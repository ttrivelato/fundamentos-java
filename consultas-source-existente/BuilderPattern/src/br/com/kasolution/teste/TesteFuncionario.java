package br.com.kasolution.teste;

import br.com.kasolution.dados.Funcionario;

public class TesteFuncionario {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Funcionario f1 = new Funcionario.Builder().build();
		Funcionario f2 = new Funcionario.Builder().codigo(100).build();
		Funcionario f3 = new Funcionario.Builder().codigo(200).nome("Rodrigo Zanquini Lopes").build();
		
		System.out.println(f1);
		System.out.println(f2);
		System.out.println(f3);

	}

}
