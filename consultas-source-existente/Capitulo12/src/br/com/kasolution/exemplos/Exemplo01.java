package br.com.kasolution.exemplos;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import br.com.kasolution.util.Formata;

public class Exemplo01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ZoneId saoPaulo = ZoneId.of("America/Sao_Paulo");
		ZoneId london = ZoneId.of("Europe/London");
		LocalDateTime reuniao = LocalDateTime.of(2018, Month.SEPTEMBER, 20, 15, 30);
		ZonedDateTime dhLondres = ZonedDateTime.of(reuniao, london);
		ZonedDateTime dhBrasil = dhLondres.withZoneSameInstant(saoPaulo);
		System.out.println("Hor�rio Londres: " + Formata.data(dhLondres));
		System.out.println("Hor�rio Brasil: " + Formata.data(dhBrasil));
		//Procurar no google DateFormatter Patterns
	}

}
