package br.com.kasolution.exemplos;

import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZonedDateTime;

import br.com.kasolution.util.Formata;

public class Exemplo02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Period p = Period.ofMonths(1).plusDays(5);
		LocalDate ld = LocalDate.now();
		LocalDate novaData = ld.plus(p);
		System.out.println("Nova Data: " + Formata.data(novaData));
		ZonedDateTime zdt = ZonedDateTime.now();
		Duration d = Duration.ofHours(5).plusMinutes(30);
		System.out.println("Nova hora: " + Formata.data(zdt.plus(d)));
	}

}
