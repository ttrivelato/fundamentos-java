package br.com.kasolution.enums;

public enum Prazo {
	SEIS_MESES(6),
	DOZE_MESES(12),
	DEZOITO_MESES(18),
	VINTEQUATRO_MESES(24),
	TRINTA_MESES(30);
	
	private final int meses;
	
	Prazo (int meses){
		this.meses = meses;
		
		
	}
	
	public int getMeses(){
		return meses;
		
	}
}
