package br.com.kasolution.dominio.contas;

import static br.com.kasolution.util.Formata.moeda;

public abstract class Conta {
	private static int ultimoCodigo = 0;
	
	private int codigo;
	private double saldo;
	private String numero;

	public Conta() {
		this.codigo = ++ultimoCodigo;
	}

	public Conta(String numero) {
		this();
		this.numero = numero;
	}
	
	public int getCodigo() {
		return codigo;
	}

	public double getSaldo() {
		return saldo;
	}

	//Pagina 101 da apostila
	void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	
	public String getNumero() {
		return numero;
	}
	
	public void depositar(double valor) {
		if (valor > 0) {
			saldo += valor;
		}else {
			System.out.println("O Valor n�o pode ser menor que 0 (zero).");
		}
		
	}

	@Override
	public String toString() {
		String info = "\tConta: " + numero;
		info +="\n\tSaldo: " + moeda(saldo);
		return info;
	}
	
	public abstract void sacar (double valor);

	

}
