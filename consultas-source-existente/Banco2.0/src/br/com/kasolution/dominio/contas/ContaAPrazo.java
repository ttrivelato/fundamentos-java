package br.com.kasolution.dominio.contas;

import static br.com.kasolution.util.Formata.data;

import java.time.LocalDate;

import br.com.kasolution.enums.Prazo;

public class ContaAPrazo extends Conta {
	private LocalDate dataMaturidade;
	
	public ContaAPrazo() {
	}
	
	public ContaAPrazo(String numero, Prazo prazo) {
		super(numero);
		setDataMaturidade(prazo);
		
	}

	public LocalDate getDataMaturidade() {
		return dataMaturidade;
	}

	public void setDataMaturidade(Prazo prazo) {
//		int meses = 0;
//		switch (prazo) {
//		case SEIS_MESES:
//			meses = 6;
//			break;
//		case DOZE_MESES:
//			meses = 12;
//			break;
//		case DEZOITO_MESES:
//			meses = 18;
//			break;
//			
//		//default : 
//		//		System.out.println("Data de maturidade inv�lida");
//		//		this.dataMaturidade = null;
//		}
		LocalDate hoje = LocalDate.now();
		this.dataMaturidade = hoje.plusMonths(prazo.getMeses());

	
	}

	
	public void sacar (double valor) {
		LocalDate hoje = LocalDate.now();
		if (hoje.isAfter(dataMaturidade)) {
		 	if (getSaldo() >= valor) { 
				setSaldo(getSaldo() - valor);
			} else {
				System.out.println("Saldo insuficiente.");
			}
		} else  {
			System.out.println("Data de Maturidade menor que a data exigida.");
		}
			
	}

	@Override
	public String toString() {
		String info = super.toString();
		info += "\nData Maturidade: " + data(dataMaturidade);
		return info;
		
	} 
	
	
	
}
	
