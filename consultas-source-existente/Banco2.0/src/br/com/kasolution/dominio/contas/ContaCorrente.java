package br.com.kasolution.dominio.contas;

import static br.com.kasolution.util.Formata.*;

public class ContaCorrente extends Conta {
	private double limiteEspecial;
	
	public ContaCorrente() {
		
	}
	
	public ContaCorrente(String numero, double limiteEspecial){
		super(numero);
		this.limiteEspecial = limiteEspecial;
		
	}

	public double getLimiteEspecial() {
		return limiteEspecial;
	}

	public void setLimiteEspecial(double limiteEspecial) {
		this.limiteEspecial = limiteEspecial;
	}

	@Override
	public String toString() {
		String info = super.toString();
		info += "\n\tLimiteEspecial: " + moeda(limiteEspecial);
		return info;
	}

	@Override
	public void sacar(double valor) {
		// TODO Auto-generated method stub
		double saldoTotal = getSaldo() + limiteEspecial;
		if (saldoTotal >= valor) {
			setSaldo(getSaldo() - valor);
		} else {
			System.out.println("Saldo Insuficiente. ");
		}
		
	}
	
	
	
}
