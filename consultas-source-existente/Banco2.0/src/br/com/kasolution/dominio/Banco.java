package br.com.kasolution.dominio;

import java.util.ArrayList;
import java.util.List;

public class Banco {
	//2 - Criar um atributo do mesmo tipo desta classe
	//e inicializa-la com uma instancia de si mesmo
	private static final Banco instance = new Banco();
	private String nome;
	private List<Cliente> clientes;
	
	//1 - Colocar o construtor Privado
	private Banco() {
		this.clientes = new ArrayList<>();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void adiciona(Cliente cliente) {
		if (!clientes.contains(cliente)) {
			clientes.add(cliente);
		}else {
			System.out.println("A pessoa informada j� � cliente.");
		}
	}
	
	public void remove(Cliente cliente) {
		if (clientes.contains(cliente)) {
			clientes.remove(cliente);
		}else {
			System.out.println("Cliente n�o localizado.");
		}
	}

	@Override
	public String toString() {
		String info = "Banco: " + nome;
		info += "\nRela��o de Clientes: ";
		for (Cliente c: clientes) {
			info += "\n" + c + "\n";
		}
		return info;
	}
	//3 - Criar um metodo getInstance para retornar o objeto;
	public static Banco getInstance() {
		return instance;
	}
	
}
