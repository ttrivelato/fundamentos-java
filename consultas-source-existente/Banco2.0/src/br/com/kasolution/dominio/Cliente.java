package br.com.kasolution.dominio;

import br.com.kasolution.dominio.contas.Conta;

public class Cliente {
	private static int ultimoCodigo = 0;
	
	private int codigo;
	private String nome;
	private String cpf;
	private Conta[] contas;
	private int qtdContas;
	
	public Cliente () {
		this.codigo = ++ultimoCodigo;
		this.cpf = "000.000.000-00";
		this.contas = new Conta[4];
	}
	
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Cliente (String nome) {
		this();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCodigo() {
		return codigo;
	}

	public Conta[] getContas() {
		return contas;
	}
		
	public void atribuiConta(Conta conta) {
		if (qtdContas < 4) {
			contas[qtdContas++] = conta;
			
		}
	}

	@Override
	public String toString() {
		String info = "Cliente: " + nome;
		info += "\nRela��o de Contas: ";
		
		for (Conta c: contas){
			if (c != null) {
			info += "\n" + c + "\n";
			}
		}
		return info;
	
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Cliente) {
			Cliente c = (Cliente) obj;
			if(this.cpf.equals(c.getCpf())) {
				return true;
			}else {
				return false;
			}
		}else
			return false;
	}
	


	

}
