package br.com.kasolution.teste;

import br.com.kasolution.dominio.Banco;
import br.com.kasolution.dominio.Cliente;

public class TesteBanco02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Cliente c1 = new Cliente("Victor Augusto");
		c1.setCpf("123.456.789-00");
		
		Cliente c2 = new Cliente ("Marcos Lima");
		c2.setCpf("123.456.789-09");
		
		Cliente c3 = new Cliente ("Marcos Lima");
		c3.setCpf("123.456.789-09");
		
		Banco banco = Banco.getInstance();
		banco.setNome("Ita�");

		banco.adiciona(c1);
		banco.adiciona(c2);
		banco.adiciona(c1);
		banco.adiciona(c3);
		
		System.out.println(banco);
	}

}
