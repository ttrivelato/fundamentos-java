package br.com.kasolution.teste;

import br.com.kasolution.dominio.Cliente;
import br.com.kasolution.dominio.contas.ContaAPrazo;
import br.com.kasolution.enums.Prazo;

public class TesteContaAPrazo {

	public static void main(String[] args) {
		ContaAPrazo c1 = new ContaAPrazo("23500-0", Prazo.SEIS_MESES);
		ContaAPrazo c2 = new ContaAPrazo("23600-1", Prazo.DOZE_MESES);
		Cliente cl1 = new Cliente("Henrique Alves");
		cl1.atribuiConta(c1);
		cl1.atribuiConta(c2);
		System.out.println(cl1.toString());
		c1.depositar(2000.00);
		c1.depositar(1500.00);
		c2.depositar(7000.00);
		System.out.println(cl1);
		c2.sacar(2595.00);
		System.out.println(cl1);
		
	}

}
