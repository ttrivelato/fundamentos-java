package br.com.kasolution.teste;

import br.com.kasolution.dominio.Cliente;
import br.com.kasolution.dominio.contas.ContaCorrente;

public abstract class TesteContaCorrente {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ContaCorrente cc1 = new ContaCorrente("54321-0", 2000.00);
		ContaCorrente cc2 = new ContaCorrente("54321-1", 3000.00);
		
		Cliente c1 = new Cliente("Rodrigo Lopes");
		c1.atribuiConta(cc1);
		c1.atribuiConta(cc2);
		System.out.println(c1);
		
		cc1.sacar(500);
		cc2.depositar(1000);
		System.out.println(c1);
		
	}

}
