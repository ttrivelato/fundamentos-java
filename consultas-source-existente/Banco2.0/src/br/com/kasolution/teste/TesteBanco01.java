package br.com.kasolution.teste;

import br.com.kasolution.dominio.Banco;
import br.com.kasolution.dominio.Cliente;

public class TesteBanco01 {

	public static void main(String[] args) {
		
		Cliente c1 = new Cliente("Victor Augusto");
		Cliente c2 = new Cliente ("Marcos Lima");
		
		Banco b1 = Banco.getInstance();
		b1.setNome("Ita�");
		b1.adiciona(c1);
		
		Banco b2 = Banco.getInstance();
		b2.setNome("Ita�");
		b2.adiciona(c2);
		
		System.out.println(b1);
		
		
		}

}
