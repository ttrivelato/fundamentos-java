package br.com.kasolution.modelo;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import br.com.kasolution.util.Formata;

public class Carrinho implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private LocalDate dataCompra;
	private List<Produto> produtos;
	private transient double totalCompra;
	
	public Carrinho(int id, LocalDate dataCompra) {
		this.produtos = new ArrayList<>();
		this.id = id;
		this.dataCompra = dataCompra;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(LocalDate dataCompra) {
		this.dataCompra = dataCompra;
	}
	
	public void adciona(Produto p) {
		produtos.add(p);
		totalCompra += p.getValor();
		
	}
	
	public void remove(Produto p) {
		produtos.remove(p);
		totalCompra -= p.getValor();
	}

	@Override
	public String toString() {
		String info = "Carrinho: " + id;
		info += "\nData Compra: " + Formata.data(dataCompra);
		info += "\nLista de itens: ";
		for (Produto p: produtos) {
			info += "\n\t" + p;
		}
		info += "\nTotal Carrinho: " + Formata.moeda(totalCompra);
		return info;
	}
	
	private void writeObject(ObjectOutputStream oos) throws IOException {
		oos.defaultWriteObject();
		oos.writeObject(ZonedDateTime.now());
	}
	private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException{
		ois.defaultReadObject();
		if(totalCompra == 0 && produtos.size() > 0) {
			totalCompra = produtos.stream()
					.mapToDouble(p -> p.getValor()).sum();
		}
		ZonedDateTime zdt = (ZonedDateTime) ois.readObject();
		System.out.println("Data geran��o arquivo: " + Formata.data(zdt));
	}
	
		
	}

