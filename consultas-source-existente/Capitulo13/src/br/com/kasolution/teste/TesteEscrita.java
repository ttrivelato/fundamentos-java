package br.com.kasolution.teste;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.LocalDate;

import br.com.kasolution.modelo.Carrinho;
import br.com.kasolution.modelo.Produto;

public class TesteEscrita {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Carrinho c = new Carrinho(1000, LocalDate.now());
		c.adciona(new Produto(1, "Mouse", 40.00));
		c.adciona(new Produto(2, "Teclado", 80.00));
		c.adciona(new Produto(3, "Monitor", 199.90));
		
		String arquivo = "C:/temp/compra"+c.getId()+".dat";
		try (FileOutputStream fos = new FileOutputStream(arquivo);
				ObjectOutputStream oos = new ObjectOutputStream(fos)){
			oos.writeObject(c);
			System.out.println("Compra Gravada com Sucesso.");
		} catch (IOException e) {
			System.out.println("Erro ao gravar: "+ e.getMessage());
		}
		
	}

}
