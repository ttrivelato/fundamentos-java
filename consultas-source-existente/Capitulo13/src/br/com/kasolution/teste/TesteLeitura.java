package br.com.kasolution.teste;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import br.com.kasolution.modelo.Carrinho;

public class TesteLeitura {

	public static void main(String[] args) {
	// TODO Auto-generated method stub
		int id = 1000;
		String arquivo = "c:/temp/compra" + id + ".dat";
		Carrinho c = null;
		try (FileInputStream fis = new FileInputStream(arquivo);
				ObjectInputStream in = new ObjectInputStream(fis)){
			c = (Carrinho) in.readObject();
			System.out.println("\n\nConteudo do arquivo: ");
			System.out.println(c);
		} catch(ClassNotFoundException | IOException e ) {
			System.out.println("Erro: " + e.getMessage());
		}
		
	}

}
