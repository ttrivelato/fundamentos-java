package br.com.kasolution.exemplos;

import java.io.IOException;

public class SimuladorErro {

	public void erro01() throws KaException {
		throw new KaException("Simula��o de Erro 01.");

	}
	
	public void erro02() throws KaSysException{
		throw new KaSysException("Simula��o de Erro 02.");
		
	}
	
	public void erro03() throws IOException {
		throw new IOException ("Simula��o de Erro 03.");
	}
}

