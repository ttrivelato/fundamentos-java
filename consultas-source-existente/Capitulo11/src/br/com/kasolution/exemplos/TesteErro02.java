package br.com.kasolution.exemplos;

import java.io.IOException;

public class TesteErro02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SimuladorErro se = new SimuladorErro();
		
		try {
			se.erro02();
			se.erro03();
		} catch (KaException | IOException e) {
			System.out.println(e.getMessage());
		}
	}

}
