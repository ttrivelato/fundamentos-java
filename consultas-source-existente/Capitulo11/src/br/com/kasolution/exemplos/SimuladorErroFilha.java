package br.com.kasolution.exemplos;

import java.io.IOException;

public class SimuladorErroFilha extends SimuladorErro{

	@Override
	public void erro01() throws KaSysException {
		// TODO Auto-generated method stub
		try {
			super.erro01();
			} catch (KaException e) {
				System.out.println("Filha: " + e.getMessage());
				throw new KaSysException("Simula��o de Erro01F.");
			}
	}

	@Override
	public void erro02() throws RuntimeException {
		// TODO Auto-generated method stub
		try{ 
			super.erro02();
		} catch (KaSysException e){
			System.out.println("Filha: " + e.getMessage());
			System.out.println("Simula��o de Erro02F.");
		}
	}

//	@Override
//	public void erro03() throws Exception {
//		// TODO Auto-generated method stub
//		super.erro03();
//	}
//	
	

}
