package br.com.kasolution.exemplos;

public class TesteErro01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SimuladorErro se = new SimuladorErro();
		SimuladorErroFilha sef = new SimuladorErroFilha();
		System.out.println("Classe pai: ");
		try {
			se.erro01();
		} catch (KaException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		System.out.println("\n\nClasse filha: ");
		try {
			sef.erro01();
		} catch (KaSysException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());

		}
		System.out.println("\n\nErro engolido filha: ");
		sef.erro02();

	}

}
