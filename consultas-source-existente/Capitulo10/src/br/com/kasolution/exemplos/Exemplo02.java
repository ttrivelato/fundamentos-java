package br.com.kasolution.exemplos;

import java.util.List;
import java.util.Optional;

import br.com.kasolution.dados.Funcionario;

public class Exemplo02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Funcionario> funcs = Funcionario.getFuncionarios();
		System.out.println("==> Funcionários ordenados por Salário: ");
		funcs.stream().sorted((f1,f2) -> f1.getSalario()
				.compareTo(f2.getSalario()))
				.forEach(f -> System.out.println(f));
		
		System.out.println("  ");
		System.out.println("==> Funcionários ordenados por Idade Decrescente: ");
		funcs.stream().sorted((f2,f1) -> f1.getIdade()
				.compareTo(f2.getIdade()))
				.map(f -> f.getNome() + " - " + f.getIdade())
				.forEach(f -> System.out.println(f));
		
		System.out.println("");
		System.out.println("==> Funcionários com maior salário: ");
		Optional<Funcionario> f = funcs.stream().max((f1,f2) -> f1.getSalario()
				.compareTo(f2.getSalario()));
		System.out.println("\t" + f.get());


		

	}

}
