package br.com.kasolution.exemplos;

import java.util.List;
import java.util.stream.Collectors;

import br.com.kasolution.dados.Funcionario;

public class Exemplo03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List <Funcionario> funcs = Funcionario.getFuncionarios();
		List <Funcionario> funcsMG;
		funcsMG = funcs.stream()
				.filter(f -> f.getEstado().equals("MG"))
				.collect(Collectors.toList());
		System.out.println("Funcionarios de MG: ");
		funcsMG.forEach(System.out::println);

	}

}
