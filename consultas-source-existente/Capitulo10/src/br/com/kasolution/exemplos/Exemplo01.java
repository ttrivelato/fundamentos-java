package br.com.kasolution.exemplos;

import java.util.List;
import java.util.Optional;

import br.com.kasolution.dados.Funcionario;
import br.com.kasolution.util.Formata;

public class Exemplo01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Funcionario> funcs = Funcionario.getFuncionarios();
		System.out.println("==> Nome das Mulheres: ");
		funcs.stream()
		.filter(f -> f.getSexo() == 'F')
		.peek(f -> System.out.print("\t"))
		.forEach(f -> System.out.println(f));
		
		System.out.println(" ");
		System.out.println("==> Soma do salario dos homens: ");
		double soma = funcs.stream()
		    .filter(f -> f.getSexo() == 'M')
		    .peek(f -> System.out.print("\t"))
			.peek(f -> System.out.println(f))
			.mapToDouble(f -> f.getSalario())
			.sum();
			System.out.println("");
			System.out.println("\t" + Formata.moeda(soma));
			System.out.println("");
			System.out.println("==> Funcionario carioca: ");
			Optional<Funcionario> fRj = funcs.stream()
					.filter(f -> f.getEstado().equals("RJ"))
					.findFirst();  //findAny() n�o deterministico
			if(fRj.isPresent()) {
				System.out.println("\t" + fRj.get().getNome());
				System.out.println("");
			} else {
				System.out.println("Nenhum Funcionario encontrado");
			}
			
			System.out.println("==> Todos Moram no RJ?");
			boolean b = funcs.stream()
					.allMatch(f -> f.getEstado().equals("RJ"));
			if(b) {
				System.out.println("SIM");
			} else {
				System.out.println("N�O");
			}
			
			
			
	}

}
