/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kasolution.teste;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Scanner;

import com.kasolution.domain.ControleFuncionario;


public class TesteBDFuncionario {

	static String pais = "BR";
	static Locale locale = Locale.getDefault();
	static ResourceBundle strings;
	
    public static void main(String[] args) throws IOException {
        // TODO code application logic here

        Scanner sc = new Scanner(System.in);
        char prompt = '\u0000';
        
        try (FileInputStream fis = new FileInputStream("settings.properties");
        		BufferedInputStream bis = new BufferedInputStream(fis)){
        	Properties p = new Properties();
        	p.load(bis);
        	pais = p.getProperty("locale");
        	switch(pais) {
        	case "FR":
        		setFranca();
        		break;
        	case "US":
        		setUS();
        		break;
        	case "CN":
        		setChina();
        		break;
        	case "RU":
        		setRussia();
        		break;
        	case "BR":
        		setBrasil();
        		break;
        	}
        }
        
        do {
            System.out.println("\n[=========== SISTEMA DE CONTROLE DE FUNCIONARIOS ===========]\n"); 
            System.out.printf("[C]%s - [A]%s - [E]%s - [B]%S - [S]%s \n",
            		strings.getString("menu1"), 
            		strings.getString("menu2"), 
            		strings.getString("menu3"), 
            		strings.getString("menu4"),
            		strings.getString("menu5"));
            System.out.print("Escolha uma das op��es do menu: ");
            prompt = sc.next().charAt(0);
            
            switch (prompt) {
                case 'C':
                case 'c': ControleFuncionario.cadastrar(); break;
                case 'A':
                case 'a': ControleFuncionario.atualizar(); break;
                case 'E':
                case 'e': ControleFuncionario.excluir(); break;
                case 'B':
                case 'b': ControleFuncionario.buscar(); break;
                case 'S':
                case 's': System.out.println("\nAt� breve!\n\n");break;
                default : System.out.println("[-OP��O INV�LIDA!-]");
            }
        } while (prompt != 'S' && prompt != 's');
    }

	private static void setBrasil() {
		// TODO Auto-generated method stub
		locale = Locale.getDefault();
		strings = ResourceBundle.getBundle("strings",locale);
	}

	private static void setRussia() {
		// TODO Auto-generated method stub
		locale = new Locale("ru", "RU") ;
		strings = ResourceBundle.getBundle("strings",locale);
	}

	private static void setChina() {
		// TODO Auto-generated method stub
		locale = Locale.CHINA;
		strings = ResourceBundle.getBundle("strings",locale);
	}

	private static void setUS() {
		// TODO Auto-generated method stub
		locale = Locale.US;
		strings = ResourceBundle.getBundle("strings",locale);
	}

	private static void setFranca() {
		// TODO Auto-generated method stub
		locale = Locale.FRANCE;
		strings = ResourceBundle.getBundle("strings",locale);
		
		
		
		
	}

}
