package com.kasolution.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.kasolution.domain.Funcionario;

public class FuncionarioDAO implements DAO<Funcionario> {

	private static final String SQL_INSERT = "INSERT INTO FUNCIONARIO (NOME, SALARIO)" + " VALUES (?,?)";
	private static final String SQL_UPDATE = "UPDATE FUNCIONARIO SET " + " NOME = ?, " + " SALARIO = ? "
			+ " WHERE CODIGO = ? ";
	private static final String SQL_DELETE = "DELETE FROM FUNCIONARIO WHERE CODIGO = ?";
	private static final String SQL_QUERY_ALL = "SELECT CODIGO, NOME, SALARIO FROM FUNCIONARIO";
	private static final String SQL_QUERY_ONE = "SELECT CODIGO, NOME, SALARIO FROM FUNCIONARIO WHERE CODIGO = ?";

	private Connection con;

	public FuncionarioDAO() {
		String url = "jdbc:mysql://localhost:3306/hr";
		String usuario = "root";
		String senha = "";
		try {
			con = DriverManager.getConnection(url, usuario, senha);
		} catch (SQLException e) {
			System.out.println("Erro: " + e.getMessage());
		}
	}

	@Override
	public void salvar(Funcionario f) throws DAOException {
		// TODO Auto-generated method stub
		try (PreparedStatement stmt = con.prepareStatement(SQL_INSERT)) {
			stmt.setString(1, f.getNome());
			stmt.setDouble(2, f.getSalario());
			if (stmt.executeUpdate() != 1) {
				throw new DAOException("Erro, insert diferente de 1!");
			}
		} catch (SQLException e) {
			throw new DAOException("Erro: " + e.getMessage());
		}

	}

	@Override
	public void atualizar(Funcionario f) throws DAOException {
		// TODO Auto-generated method stub
		try (PreparedStatement stmt = con.prepareStatement(SQL_UPDATE)) {
			stmt.setString(1, f.getNome());
			stmt.setDouble(2, f.getSalario());
			stmt.setInt(3, f.getCodigo());
			if (stmt.executeUpdate() != 1) {
				throw new DAOException("Erro, update diferente de 1!");
			}
		} catch (SQLException e) {
			throw new DAOException("Erro: " + e.getMessage());
		}
	}

	@Override
	public boolean excluir(Funcionario f) throws DAOException {
		// TODO Auto-generated method stub
		try (PreparedStatement stmt = con.prepareStatement(SQL_DELETE)) {
			stmt.setInt(1, f.getCodigo());
			return stmt.execute();
		} catch (SQLException e) {
			throw new DAOException("Erro: " + e.getMessage());
		}

	}
	
	private Funcionario ormFuncionario(ResultSet rs) throws SQLException {
		Funcionario f = new Funcionario.Builder()
				.codigo(rs.getInt("CODIGO"))
				.nome(rs.getString("NOME"))
				.salario(rs.getDouble("SALARIO"))
				.build();
		return f;
	}

	@Override
	public Funcionario buscarId(int id) throws DAOException {
		// TODO Auto-generated method stub
		Funcionario f = null;
		try (PreparedStatement stmt = con.prepareStatement(SQL_QUERY_ONE)) {
			stmt.setInt(1, id);
			try(ResultSet rs = stmt.executeQuery()){
				if (rs.next()) {
					f = ormFuncionario(rs);
				}
			}
			return f;
		} catch (SQLException e) {
			throw new DAOException("Erro: " + e.getMessage());
		}
	}

	@Override
	public List<Funcionario> buscarTodos() throws DAOException {
		// TODO Auto-generated method stub
		List<Funcionario> fs = new ArrayList<>();
		try (PreparedStatement stmt = con.prepareStatement(SQL_QUERY_ALL)) {
			try(ResultSet rs = stmt.executeQuery()){
				while (rs.next()) {
					fs.add(ormFuncionario(rs));
				}
			}
			return fs;
		} catch (SQLException e) {
			throw new DAOException("Erro: " + e.getMessage());
		}
	}

	@Override
	public void close() throws DAOException {
		// TODO Auto-generated method stub
		try {
			con.close();
		} catch (SQLException e) {
			System.out.println("Erro: " + e.getMessage());
		}

	}

}
