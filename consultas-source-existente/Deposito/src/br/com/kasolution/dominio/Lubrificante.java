package br.com.kasolution.dominio;

import br.com.kasolution.util.Formata;

public abstract class Lubrificante implements ItemCalculavel{
	private double litros;
	private double valorLitro;

	public double getLitros() {
		return litros;
	}
	public void setLitros(double litros) {
		this.litros = litros;
	}
	public double getValorLitro() {
		return valorLitro;
	}
	public void setValorLitro(double valorLitro) {
		this.valorLitro = valorLitro;
	}
	
	
	@Override
	public String toString() {
		String info = "\nLubrificante: " + getClass().getSimpleName();
		info += "\n\tLitros: " + litros;
		info += "\n\tValor litro: " + Formata.moeda(valorLitro);
		info += "\n\tTotal: " + Formata.moeda(litros*valorLitro);
		return info;
	}
	
}
