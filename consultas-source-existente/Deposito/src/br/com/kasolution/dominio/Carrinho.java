package br.com.kasolution.dominio;

import java.util.ArrayList;
import java.util.List;

import br.com.kasolution.util.Formata;

public class Carrinho {
	
	private List<ItemCalculavel> itens = new ArrayList<>();

	public void adiciona(ItemCalculavel item) {
		itens.add(item);
	}
	
	public void remove(ItemCalculavel item) {
		itens.remove(item);
	}

	@Override
	public String toString() {
		double totalCarrinho = 0;
		String info = "Itens Carrinho: ";
		for (ItemCalculavel i : itens) {
			info +="\n" + i + "\n";
			totalCarrinho += i.calculaTotal();
		}
		info += "\n\tTotal Carrinho: " + Formata.moeda(totalCarrinho);
		return info;
	}
	
	
}
