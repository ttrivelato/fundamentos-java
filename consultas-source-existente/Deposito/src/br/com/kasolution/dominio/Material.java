package br.com.kasolution.dominio;

import br.com.kasolution.util.Formata;

public abstract class Material {
	private double quantidade;
	private double valorUnitario;

	
	public double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(double quantidade) {
		this.quantidade = quantidade;
	}
	public double getValorUnitario() {
		return valorUnitario;
	}
	public void setValorUnitario(double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}
	
	@Override
	public String toString() {
		String info = "\nMaterial: " + getClass().getSimpleName();
		info += "\n\tQuantidade: " + quantidade;
		info += "\n\tValor unit�rio: " + Formata.moeda(valorUnitario);
		info += "\n\tTotal: " + Formata.moeda(quantidade*valorUnitario);
		return info;
	}
	
	
	
	
	
	
}
