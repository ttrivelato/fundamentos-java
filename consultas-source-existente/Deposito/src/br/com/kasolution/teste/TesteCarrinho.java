package br.com.kasolution.teste;

import br.com.kasolution.dominio.Carrinho;
import br.com.kasolution.dominio.LataGraxa;
import br.com.kasolution.dominio.LoteTijolo;

public class TesteCarrinho {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LoteTijolo lt = new LoteTijolo();
		lt.setQuantidade(500);
		lt.setValorUnitario(.50);

		LataGraxa lg = new LataGraxa();
		lg.setLitros(1000);
		lg.setValorLitro(1.50);
		
		Carrinho c = new Carrinho();
		c.adiciona(lt);
		c.adiciona(lg);
		
		System.out.println(c);

	}

}
