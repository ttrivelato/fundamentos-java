package br.com.kasolution.exemplo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.com.kasolution.dados.Funcionario;

public class ExemploMap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Funcionario> funcs = Funcionario.getFuncionarios();
		Map<Integer, Funcionario> map = new HashMap<>();
		for(Funcionario f : funcs) {
			map.put(f.getCodigo(), f);
		}
		System.out.println("Funcion�rio c�digo 10: " );
		System.out.println(map.get(10));
		Set<Integer> keys = map.keySet();
		keys.forEach(i -> System.out.println("[" + i +"] - " + map.get(i)));

	}

}
