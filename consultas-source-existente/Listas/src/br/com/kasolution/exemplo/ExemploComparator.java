package br.com.kasolution.exemplo;

import java.util.Collections;
import java.util.List;

import br.com.kasolution.dados.Funcionario;

public class ExemploComparator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Funcionario> funcs = Funcionario.getFuncionarios();
		Collections.sort(funcs, new OrndenaIdade());
		funcs.forEach((Funcionario f) -> System.out.println(f));
		Collections.sort(funcs, ( f1,  f2) -> f1.getSalario().compareTo(f2.getSalario()));
		System.out.println("Ordena��o pelo Sal�rio: ");
		funcs.forEach(f -> System.out.println(f));
		Collections.sort(funcs, (f1,f2) -> f1.getEstado().compareTo(f2.getEstado()));
		System.out.println("Ordena��o pelo Estado: ");
		funcs.forEach(f -> System.out.println(f));
	}

}
