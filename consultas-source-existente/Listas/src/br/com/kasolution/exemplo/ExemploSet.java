package br.com.kasolution.exemplo;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import br.com.kasolution.dados.Funcionario;

public class ExemploSet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Funcionario> funcs = Funcionario.getFuncionarios();
		Set<Funcionario> funcsOrdenado = new TreeSet<>();
		funcsOrdenado.addAll(funcs);
		for (Funcionario f : funcsOrdenado) {
			System.out.println(f);
		}
	}

}
