package br.com.kasolution.teste;

import br.com.kasolution.dominio.Banco;
import static br.com.kasolution.dominio.Banco.CORRECAO_MENSAL;
import br.com.kasolution.dominio.Conta;
import static br.com.kasolution.simuladores.Simulador.simulaAplicacaoPromocional;
import br.com.kasolution.util.Dados;
import static br.com.kasolution.util.Formata.moeda;

public class TesteSimulaPromocao {

    public static void main(String[] args) {
        Banco banco = Dados.getDados();
        double valorDesejado = 14_000.00;
        System.out.println("Simulador de promoção:\n");
        
        for (Conta c : banco.getContas()) {
            int meses = simulaAplicacaoPromocional(
                    c, CORRECAO_MENSAL, valorDesejado);
            
            if (meses == 0) {
                System.out.println("Não ligar para o"
                        + " cliente: " + c.getCliente()
                                .getNome());
            } else {
                System.out.println("Promção atiginir valor de: "
                        + moeda(valorDesejado)
                        + " Ligar para "
                        + c.getCliente().getNome()
                        + " ele possui saldo de: " +moeda(c.getSaldo())
                        + " e oferecer promoção de "
                        + meses + " mese(s).");
            }//fim if
        }//fim for
    }//fim main
    
}
