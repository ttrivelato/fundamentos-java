package br.com.kasolution.teste;

import br.com.kasolution.dominio.Banco;
import br.com.kasolution.dominio.Conta;
import static br.com.kasolution.simuladores.Simulador.*;
import br.com.kasolution.util.Dados;
import static br.com.kasolution.util.Formata.moeda;

public class TesteSimuladorMensal {

    public static void main(String[] args) {
        Banco banco = Dados.getDados();
        Conta conta = banco.getConta(2);//Renato
                       
        System.out.println("Saldo: " + moeda(conta.getSaldo()));
        
        double valorFinalUmMes = 
                simulaAplicacaoMensal(conta,
                        Banco.CORRECAO_MENSAL, 1);
        System.out.println("Cliente: " + conta
                .getCliente().getNome());
        
        System.out.println("Simulação de investimento "
                + "para " + 1 + " mes: "
                + moeda(valorFinalUmMes));
        
        double valorFinalSeteMeses = 
                simulaAplicacaoMensal(conta,
                        Banco.CORRECAO_MENSAL, 7);
        System.out.println("Cliente: " + conta
                .getCliente().getNome());
        
        System.out.println("Simulação de investimento "
                + "para " + 7 + " meses: "
                + moeda(valorFinalSeteMeses));
        
        double valorFinalDozeMeses = 
                simulaAplicacaoMensal(conta,
                        Banco.CORRECAO_MENSAL, 12);
        System.out.println("Cliente: " + conta
                .getCliente().getNome());
        
        System.out.println("Simulação de investimento "
                + "para " + 12 + " meses: "
                + moeda(valorFinalDozeMeses));
        
        //=========
                
        Conta conta2 = banco.getConta(1);//Renato
                       
        System.out.println("");
        System.out.println("----------");
        System.out.println("");
        
        System.out.println("Saldo: " + moeda(conta2.getSaldo()));
        
        double valorFinalUmMes2 = 
                simulaAplicacaoMensal(conta2,
                        Banco.CORRECAO_MENSAL, 1);
        System.out.println("Cliente: " + conta2
                .getCliente().getNome());
        
        System.out.println("Simulação de investimento "
                + "para " + 1 + " mes: "
                + moeda(valorFinalUmMes2));
        
        double valorFinalSeteMeses2 = 
                simulaAplicacaoMensal(conta2,
                        Banco.CORRECAO_MENSAL, 7);
        System.out.println("Cliente: " + conta2
                .getCliente().getNome());
        
        System.out.println("Simulação de investimento "
                + "para " + 7 + " meses: "
                + moeda(valorFinalSeteMeses2));
        
        double valorFinalDozeMeses2 = 
                simulaAplicacaoMensal(conta2,
                        Banco.CORRECAO_MENSAL, 12);
        System.out.println("Cliente: " + conta2
                .getCliente().getNome());
        
        System.out.println("Simulação de investimento "
                + "para " + 12 + " meses: "
                + moeda(valorFinalDozeMeses2));
    }    
}