package br.com.kasolution.relatorio;

import br.com.kasolution.dados.Dados;
import br.com.kasolution.dominio.Funcionario;
import static br.com.kasolution.util.Formata.moeda;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;

public class RelatorioFuncionarios {

    static File arquivo = new File("c:/temp/rel_funcionario.txt");
    static PrintStream ps = System.out;

    public static void geraArquivoFuncionario() {
        
        try {
            
            System.setOut(new PrintStream(arquivo));
            ArrayList<Funcionario> funcionarios = Dados.getFuncionarios();
            
            for (Funcionario f : funcionarios) {
                f.imprime();
                System.out.println("Salário: " + moeda(f.calculaValorPagamento()));
                System.out.println();
            }
            
            System.setOut(ps);
            System.out.println("Arquivo gerado!");
            
        } catch (FileNotFoundException e) {
            System.out.println("Erro ao criar o arquivo. " + e.getMessage());
        } finally {
            ps.close();
        }
    }
}