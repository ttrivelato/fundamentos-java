package br.com.kasolution.relatorio;

import br.com.kasolution.dados.Dados;
import br.com.kasolution.dominio.Fatura;
import br.com.kasolution.dominio.Funcionario;
import br.com.kasolution.dominio.Pagavel;
import static br.com.kasolution.util.Formata.moeda;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;

public class RelatorioOrdemPagamento {
    
    
    static File arquivo = new File("c:/temp/rel_ordempagamento.txt");
    static PrintStream ps = System.out;

    public static void geraArquivoOrdemPagamento() throws FileNotFoundException {
        
        System.setOut(new PrintStream(arquivo));
        
        ArrayList<Pagavel> pagaveis = new ArrayList<>();
        pagaveis.addAll(Dados.getFuncionarios());
        
        pagaveis.add(new Fatura("Computador", 4, 4000));
        pagaveis.add(new Fatura("Xinxila albina", 2, 3256));
        System.out.println("*** Ordem de Pagamento ***");
        double total = 0;
        
        for (Pagavel p: pagaveis) {
            if (p instanceof Funcionario) {
                System.out.println("==> Pagamento de Funcionario");
            } else {
                System.out.println("==> Pagamento de Fatura");
            }
            p.imprime();
            double valorPagamento = p.calculaValorPagamento();
            System.out.println("Valor pagamento: "+ moeda(valorPagamento) + "\n");
            total += valorPagamento;
        }//fim for
        System.out.println("Total Ordem Pag.:" + moeda(total));
        
        System.setOut(ps);
        System.out.println("Arquivo gerado!");
    }    
}