package br.com.kasolution.teste;

import br.com.kasolution.relatorio.RelatorioOrdemPagamento;
import java.io.FileNotFoundException;

public class TesteRelatorioOp {

    public static void main(String[] args) {
        
        try {
            RelatorioOrdemPagamento.geraArquivoOrdemPagamento();
        } catch (FileNotFoundException e) {
             System.out.println(TesteRelatorioOp.class.getName() + e.getMessage());
        }
    }
}
