package br.com.kasolution.teste;

import br.com.kasolution.dominio.Aluno;

public class TesteAluno {
    
    public static void main(String[] args) {
        
        Aluno[] alunos = new Aluno[10];
        
        alunos[0] = new Aluno();
        alunos[0].nome = "Jonas Bock";
        alunos[0].idade = 11;
        
        alunos[1] = new Aluno();
        alunos[1].nome = "Pedro Paulo";
        alunos[1].idade = 55;
        
        alunos[2] = new Aluno();
        alunos[2].nome = "João Avelange";
        alunos[2].idade = 8;
        
        alunos[3] = new Aluno();
        alunos[3].nome = "Martinho da Vila";
        alunos[3].idade = 10;
        
        alunos[4] = new Aluno();
        alunos[4].nome = "Carla Sonia";
        alunos[4].idade = 26;
        
        alunos[5] = new Aluno();
        alunos[5].nome = "Marleta Serafin";
        alunos[5].idade = 25;        
        
        alunos[6] = new Aluno();
        alunos[6].nome = "João Gilberto Silva";
        alunos[6].idade = 33;
                
        alunos[7] = new Aluno();
        alunos[7].nome = "Jesus Cristo È o Deminio";
        alunos[7].idade = 33;
        
        alunos[8] = new Aluno();
        alunos[8].nome = "Satanas Gera Caos";
        alunos[8].idade = 12;
        
        alunos[9] = new Aluno();
        alunos[9].nome = "Globo mama";
        alunos[9].idade = 26;
        
        int contador = 0;
        for (Aluno a : alunos) {
            
            if (a.idade >= 12) {
                System.out.println("[Nome]: " + a.nome);
                System.out.println("[Idade]: "+ a.idade);
                System.out.println("");
                contador++;
            }
            
            if (contador == 5) {
                break;
            }
        }
    }
}