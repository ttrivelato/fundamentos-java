package br.com.kasolution.dominio;

public class Boletim {

    public Aluno alunos[];

    public Boletim(Aluno alunos[]) {
        this.alunos = alunos;
    }
    
    public double calculaMedia(Aluno aluno) {
        double total = 0, media;
        for (double n : aluno.notas) {
            total += n;
        }
        media = total/aluno.notas.length;
        return media;
    }
    
    public String obtemSituacao(double notaMinimaAprovacao, double media) {
        String situacao;
        
        if (media >= notaMinimaAprovacao) {
            situacao = "Aprovado";
        } else {
            situacao = "Reprovado";
        }
        
        return situacao;
    }
    
    public void imprime() {
        for (Aluno a : alunos) {
            a.imprime();
            double media = calculaMedia(a);
            System.out.println("Média: " + media);
            
            String situacao = obtemSituacao(a.notaMinimaAprovacao, media);
            
            System.out.println("Situação aluno: ****** " + situacao + " ******\n");
        }
    }
}