package br.com.kasolution.teste;

import br.com.kasolution.dominio.Aluno;
import br.com.kasolution.dominio.Boletim;

public class TesteBoletim {

    public static void main(String[] args) {
        Aluno[] alunos = {
          new Aluno("Paulo", 6.5, 7.6, 7.7, 7.0),
          new Aluno("Pedro", 6.5, 5.6, 5.7, 5.0),
          new Aluno("Plinio", 6.5, 5.6, 5.7, 5.0),
          new Aluno("Priscila", 5.5, 5.6, 5.7, 5.0),
          new Aluno("Patricia", 5.5, 5.6, 5.7, 5.0)
        };
        
        //alunos[2].notaMinimaAprovacao = 5.0;
        //Aluno.notaMinimaAprovacao = 7.0;
        
        Boletim boletim = new Boletim(alunos);
        boletim.imprime();
    }    
}