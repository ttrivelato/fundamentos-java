package br.com.kasolution.teste;

import br.com.kasolution.dominio.Mes;
import java.util.Scanner;

public class TesteMes {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Informe o número do mes [1-12]: ");
        
        //Pega valor do scanner
        int mesInt = sc.nextInt();        
        
        if(mesInt >= 1 && mesInt <= 12) {
            
            Mes mes = new Mes();            
            System.out.println("Mes escolhido: "+mesInt+" - "+mes.getNomeMes(mesInt));
            
        } else {
            System.out.println("Dia inválido!");
        }        
    }
}