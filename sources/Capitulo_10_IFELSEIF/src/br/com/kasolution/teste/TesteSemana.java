package br.com.kasolution.teste;

import br.com.kasolution.dominio.Semana;
import java.util.Scanner;

public class TesteSemana {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Informe o número do dia [1-7]: ");
        
        //Pega valor do scanner
        int dia = sc.nextInt();
        
        if(dia >= 1 && dia <= 7) {
            
            Semana semana = new Semana();            
            System.out.println("Dia escolhido Ifelse: "+dia+" - "+semana.getNomeDiaIfElse(dia));
            System.out.println("Dia escolhido Switch: "+dia+" - "+semana.getNomeDiaIfSwitch(dia));
        } else {
            System.out.println("Dia inválido!");
        }        
    }
}