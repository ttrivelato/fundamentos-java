package br.com.kasolution.dominio;

public class Semana {
    
    
    public String getNomeDiaIfElse(int dia) {
        String nomeDia;
        
        if (dia == 1) {
            nomeDia = "Domingo";
        } else if (dia == 2) {
            nomeDia = "Segunda";
        } else if (dia == 3) {
            nomeDia = "Terça";
        } else if (dia == 4) {
            nomeDia = "Quarta";
        } else if (dia == 5) {
            nomeDia = "Quinta";
        } else if (dia == 6) {
            nomeDia = "Sexta";
        } else if (dia == 7) {
            nomeDia = "Sábado";
        } else {
            nomeDia = "Dia inválido!";
        }
        
        return nomeDia;
    }  
    
    public String getNomeDiaIfSwitch(int dia) {
        String nomeDia;
        
        switch(dia) {
                case 1:
                    nomeDia = "Domingo";
                    break;
                case 2:
                    nomeDia = "Segunda";
                    break;
                case 3:
                    nomeDia = "Terça";
                    break;
                case 4:
                    nomeDia = "Quarta";
                    break;                
                case 5:
                    nomeDia = "Quinta" ;
                    break;
                case 6:
                    nomeDia = "Sexta";
                    break;
                case 7:
                    nomeDia = "Sábado";
                    break;
                    
                default:
                    nomeDia = "Dia inválido!";
                    break;
            }
        
        return nomeDia;
    }
}