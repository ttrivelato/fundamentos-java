package br.com.kasolution.teste;

public class TesteString {

    public static void main(String[] args) {
        
        String frase = "Curso Java Ka Solution - Centro";
        String maiscula = frase.toUpperCase();
        System.out.println(maiscula);
        
        String minuscula = frase.toLowerCase();
        System.out.println(minuscula);
        
        System.out.println(frase.concat("!"));
        
        String unidade = frase.substring(25, 31);
        System.out.println(unidade);
        
        int tamanho = frase.length();
        System.out.println("Tamanho da string: "+tamanho);
        
        int posicaoKa = frase.indexOf("Ka");
        System.out.println("Pos. Ka " +posicaoKa);
        
        frase = frase.replace("Centro", "Morumbi");
        System.out.println(frase);
        
        char caracter11 = frase.charAt(11);
        System.out.println("Caracter 11 " +caracter11);
        
        String semEspaco = "           OK            ".trim();
        System.out.println("{" + semEspaco + "}");
        
        boolean contemKa = frase.contains("Ka");
        System.out.println("Contem KA " +contemKa);
    }
}