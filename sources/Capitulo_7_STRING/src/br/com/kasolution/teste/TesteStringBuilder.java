package br.com.kasolution.teste;

public class TesteStringBuilder {

    public static void main(String[] args) {
        
        StringBuilder frase = new StringBuilder("Thiago");
        frase.append(" Aluno");
        frase.insert(6, " Trivelato ");
        frase.append("!");
        System.out.println(frase);
        frase.insert(6, " Trivelato ");
        frase.reverse();
        System.out.println(frase);
    }
}