package br.com.kasolution.teste;

import br.com.kasolution.dominio.Produto;
import java.util.Scanner;

public class TesteVenda {

    public static void main(String[] args) {
        
        Produto p = new Produto();      
        Scanner sc = new Scanner(System.in);
                
        System.out.print("Informe a quantidade da venda: ");
        int qdtVendida = sc.nextInt();
        
        if(qdtVendida > p.quantidade) {
            System.out.println("Quantidade insufiencente verifique a quantidade no estoque");
            System.out.println("Quantidade atual do estoque: "+p.quantidade);
        } else {
            System.out.println("Venda finalizada com sucesso!");
            System.out.println("Produto: "+ p.nome);
            System.out.println("Valor total da venda: "+ (qdtVendida * p.valorUnitario));
            System.out.println("Obrigado pela compra volte sempre");
        }
    }
}
