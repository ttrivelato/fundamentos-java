package br.com.kasolution.dominio;

public class Produto {

    public int codigo;
    public String nome;
    public double valor;

    public void imprime() {
        System.out.println("Produto: " + codigo);
        System.out.println("Descrição: " + nome);
        System.out.println("Valor: " + valor);
    }
}