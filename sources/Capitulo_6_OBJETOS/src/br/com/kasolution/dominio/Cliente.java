package br.com.kasolution.dominio;

public class Cliente {

    public int codigo;
    public String nome;
    public String enderecoEntrega;

    public void imprime() {
        System.out.println("Cliente: #" + codigo);
        System.out.println("Nome: " + nome);
        System.out.println("End.Entrega: " + enderecoEntrega);
    }
}
