package br.com.kasolution.teste;

import br.com.kasolution.dominio.Cliente;
import br.com.kasolution.dominio.Produto;
import br.com.kasolution.dominio.Venda;
import java.util.Date;
import java.util.Scanner;

public class TesteVenda {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("[ e-commerce - Mkp Place ]\n\n");
        
        Cliente cliente = new Cliente();
        System.out.print("Cod.Cliente: ");        
        cliente.codigo = sc.nextInt();
        
        System.out.print("Nome: ");
        cliente.nome = sc.next();
        
        System.out.print("Rua: ");
        cliente.enderecoEntrega = sc.next();
        
        System.out.print("Num. rua: ");
        cliente.enderecoEntrega += ", " + sc.nextInt();
        
        System.out.print("Quantos produtos: ");
        int qtdProdutos = sc.nextInt();

        Venda venda = new Venda();
        System.out.print("Cod.Venda: ");
        venda.codigo = sc.nextInt();
        
        venda.data = new Date(); 
        venda.cliente = cliente;
        
        venda.produtos = new Produto[qtdProdutos];
        
        for (int i = 0;i < qtdProdutos; i++) {
            venda.produtos[i] = new Produto();
            System.out.print("Cod.Produto: ");
            venda.produtos[i].codigo = sc.nextInt();
            System.out.print("Descrição: ");
            venda.produtos[i].nome = sc.next();
            System.out.print("Valor: ");
            venda.produtos[i].valor = sc.nextDouble();
        } 
        
        venda.calculaTotal();       
        venda.imprime();
    }
}