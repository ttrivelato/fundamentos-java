package br.com.kasolution.teste;

import br.com.kasolution.dominio.Semana;
import java.util.Scanner;


public class TesteSemana {

    public static void main(String[] args) {
        
        Semana s = new Semana();
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Para apresentar o dia da semana digite de 0 a 6: ");
        System.out.println("0: Sabado");
        System.out.println("1: Domingo");
        System.out.println("2: Segunda");
        System.out.println("3: Terça");
        System.out.println("4: Quarta");
        System.out.println("5: Quinta");
        System.out.println("6: Sexta");
        
        int diaSemana = sc.nextInt();
        
        //Valida dia da semana
        if(diaSemana < 0 || diaSemana > 6) {
            System.out.println("Dia da semana deve estar entre 0 e 6 ");
        } else {
            System.out.println("O dia da semana escolhido foi: "+ s.semana[diaSemana]);
        }        
    }    
}