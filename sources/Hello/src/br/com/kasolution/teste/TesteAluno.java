package br.com.kasolution.teste;

import br.com.kasolution.dominio.Aluno;

public class TesteAluno {

    public static void main(String[] args) {
        System.out.println("br.com.kasolution.teste.TesteAluno.main()");
        
        Aluno aluno1 = new Aluno();        
        aluno1.nome = "joao de deus";
        aluno1.idade = 17;
        
        System.out.println("Nome1: "+aluno1.nome);
        System.out.println("Idade1: "+aluno1.idade);
        
        Aluno aluno2 = new Aluno();        
        aluno2.nome = "jose de deus";
        aluno2.idade = 22;
        
        System.out.println("Nomes: "+aluno2.nome);
        System.out.println("Idade2: " +aluno2.idade);
    }
}